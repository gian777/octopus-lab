function httpGet()
{
    var url = document.getElementById("url").value;
    if(url != ""){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            word_cloud(JSON.parse(xmlHttp.responseText));
    };
    xmlHttp.open("GET", "/index?url=" + url, true);
    xmlHttp.send(null);
            };
};

window.onload = function(){
var button = document.getElementById('search');
button.onclick = httpGet;
}

function word_cloud(words){
    WordCloud(document.getElementById('wordcloud'), { rotateRatio: 0.5, list: words } );
}
