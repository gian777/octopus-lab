create database word_cloud;

use word_cloud;

create table top_words(hash VARCHAR(255) NOT NULL, word VARCHAR(255) NOT NULL, frequency integer, PRIMARY KEY (hash));