"""

"""
import re
from bs4 import BeautifulSoup
from bs4 import Comment
from collections import Counter
import requests
from string import punctuation
from nltk.corpus import stopwords

AVOIDED_TAGS = ['style', 'script', 'head', 'title', 'meta', '[document]']


class WordsCounter(object):
    """

    """

    def __init__(self, url):
        """

        :param url:
        """
        self.url = url

    def get_html_content(self, url):
        """

        :param url:
        :return:
        """
        request = requests.get(url)
        return request.content

    @staticmethod
    def is_significant_element(element):
        """

        :param element:
        :return:
        """
        if element.parent.name in AVOIDED_TAGS:
            return False
        if isinstance(element, Comment):
            return False
        return True

    def find_all_text(self):
        """

        :return:
        """
        html_content = self.get_html_content(self.url)
        beautiful_soup = BeautifulSoup(html_content, "html.parser")
        texts = beautiful_soup.findAll(text=True)
        return texts

    def count_words_occurrences(self):
        """

        :return:
        """
        texts = self.find_all_text()
        relevant_texts = self.clean_filter_texts(texts)
        return Counter((word.strip(punctuation).lower() for phrase in relevant_texts
                        for word in phrase.split() if self.is_valid_word(word.strip(punctuation).lower())))

    @staticmethod
    def is_valid_word(cleaned_word):
        """

        :param cleaned_word:
        :return:
        """
        return cleaned_word not in stopwords.words("english") and re.match("^[a-zA-Z]+$", cleaned_word) and not len(cleaned_word) < 2

    def clean_filter_texts(self, texts):
        """

        :param texts:
        :return:
        """
        relevant_texts = filter(self.is_significant_element, texts)
        return relevant_texts

