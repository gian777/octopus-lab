crypto==1.4.1
tornado==4.5.3
bs4==0.0.1
requests==2.18.1
nltk==3.2.5
webapp2==2.5.2
mysqlclient==1.3.12