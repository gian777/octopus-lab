## Project overview

The project is a Tornado web server and it can be used with the following routes:

/index
/admin

the first page is the main page of the application and it's possible to fetch the url desired and visualize the 
word cloud in the same page. On the same time, the data are stored in the database with an INSERT/UPDATE depending if
the word already exist in the table.(store the word encrypted and the key as the word hash)

From the second url(/admin) it's possible to visualize the data inside the database table.(decrypted words)

Unfortunately, at the end of the project I tried to deploy the project with google app engine but I received the 
error google.appengine.api.yaml_errors.EventError: the library "bs4" is not supported I haven't had any chance to
deploy the project on the cloud.

---

## Installation instruction

To run the project it's necessary to add Tornado in a local folder. it necessary to run the following command:

pip install -t lib/ tornado

The other requirements are listed in the requirements.txt:

pip install -r requirements.txt