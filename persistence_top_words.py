"""

"""
import hashlib
import os

import MySQLdb
from configparser import ConfigParser
from Crypto.PublicKey import RSA

PEM_PATH = os.getcwd() + "/key.pem"

INSERT_UPDATE = ("INSERT INTO top_words VALUES(%s, %s, %s)"
                 " ON DUPLICATE KEY UPDATE frequency=values(count)+%s;")

SELECT_TOP_WORDS = "SELECT word FROM top_words order by frequency desc;"


def get_db_config(filename):
    parser = ConfigParser()
    parser.read(filename)
    return parser

parser = get_db_config('config.cfg')


def make_word_hash(word):
    """

    :param word:
    :return:
    """
    hash_object = hashlib.md5(word)
    return hash_object.hexdigest()


class StoreTopWords(object):
    """

    """
    def __init__(self):
        """

        """
        with open(PEM_PATH) as f:
            self.pem_key = RSA.importKey(f)

    def encrypt(self, word):
        return self.pem_key.publickey().encrypt(word, 32)

    def decrypt(self, encrypted_word):
        return self.pem_key.decrypt(encrypted_word)

    def connect(self):
        """

        :return:
        """
        if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
            return MySQLdb.connect(unix_socket='/cloudsql/' + parser.get('db', 'socket_name'),
                                   user=parser.get('db', 'username'),
                                   passwd=parser.get('db', 'password'),
                                   db=parser.get('db', 'name')
                                   )
        else:
            return MySQLdb.connect(host='localhost',
                                   user=parser.get('db', 'username'),
                                   passwd=parser.get('db', 'password'),
                                   db=parser.get('db', 'name')
                                   )

    def insert_update_top_words(self, words_frequencies):
        """

        :param words_frequencies:
        :return:
        """
        connection = self.connect()
        try:
            connection.autocommit = False
            with connection.cursor() as cursor:
                for pair in words_frequencies:
                    cursor.execute(INSERT_UPDATE, make_word_hash(pair[0]), self.encrypt(pair[0]), pair[1], pair[1])
            connection.commit()
        finally:
            connection.close()

    def get_top_words(self):
        """

        :return:
        """
        connection = self.connect()
        try:
            connection.autocommit = False
            with connection.cursor() as cursor:
                cursor.execute(SELECT_TOP_WORDS)
                result = cursor.fetchall()
                print(result)
                return [] if not result else [(self.decrypt(word), result) for word, frequency in result]
        finally:
            connection.close()
