from os.path import dirname, abspath
import json
import tornado.web
from words_counter import WordsCounter
from persistence_top_words import StoreTopWords
from tornado import web, wsgi

LIMIT = 100


class IndexHandler(tornado.web.RequestHandler):
    """

    """
    def get(self):
        if not self.request.arguments:
            main_directory = dirname(abspath(__file__))
            self.render(main_directory + "/static/index.html")
        elif self.request.arguments["url"]:

            wrapped_url = self.get_argument("url", False)
            word_counter = WordsCounter(wrapped_url)
            count_occurrences = word_counter.count_words_occurrences().most_common(LIMIT)
            print(count_occurrences)
            store_top_words = StoreTopWords().insert_update_top_words(count_occurrences)
            print(store_top_words)
            self.write(json.dumps(count_occurrences))


class AdminHandler(tornado.web.RequestHandler):
    """

    """
    def get(self):
        main_directory = dirname(abspath(__file__))
        store_top_words = StoreTopWords()
        top_words = store_top_words.get_top_words()
        self.render(main_directory + "/static/admin.html", words=top_words)


class Application(web.Application):

    def __init__(self):
        handlers = [
            (r"/index", IndexHandler),
            (r"/admin", AdminHandler),
            (r'/(.*)$', tornado.web.StaticFileHandler, {'path': 'static', 'default_filename': 'index.html'}),
        ]
        web.Application.__init__(self, handlers)

application = wsgi.WSGIAdapter(Application())
